import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';
import 'package:notse/text_field.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  late TextEditingController _nameTextEditingController;
  late TextEditingController _lastNameTextEditingController;
  late TextEditingController _emailTextEditingController;
  late TextEditingController _phoneTextEditingController;
  late TextEditingController _passwordTextEditingController;
  String? _emailError;
  String? _passwordError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameTextEditingController = TextEditingController();
    _lastNameTextEditingController = TextEditingController();
    _emailTextEditingController = TextEditingController();
    _phoneTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image(
            image: AssetImage('images/launch.png'),
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Positioned(
           left: SizeConfig.scaleWidth(33),
            top: SizeConfig.scaleWidth(82),
              child: IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () {
                    Navigator.pushNamed(context, '/sign_in');
                  })),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Sign Up',
                style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: SizeConfig.scaleHeight(30),
                  fontWeight: FontWeight.w700,
                  color: Color(0XFF23203F),
                ),
              ),
              Text(
                'Create an account',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: SizeConfig.scaleHeight(18),
                  fontWeight: FontWeight.w300,
                  color: Color(0XFF9391A4),
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(53),
              ),
              Container(
                width: SizeConfig.scaleWidth(323),
                height: SizeConfig.scaleHeight(351),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(9),
                  color: Colors.white.withOpacity(.2),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      right: 15, top: 32, bottom: 32, left: 15),
                  child: Column(
                    children: [
                      MyTextField(
                        textEditingController: _nameTextEditingController,
                        hint: 'Kendrick',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w400,
                        color: Color(0XFF23203F),
                      ),
                      MyTextField(
                        textEditingController: _lastNameTextEditingController,
                        hint: 'Last name',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w300,
                        color: Color(0XFF9391A4),
                      ),
                      MyTextField(
                        textEditingController: _emailTextEditingController,
                        hint: 'Email',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w300,
                        color: Color(0XFF9391A4),
                      ),
                      MyTextField(
                        textEditingController: _phoneTextEditingController,
                        hint: 'Phone',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w300,
                        color: Color(0XFF9391A4),
                      ),
                      MyTextField(
                        textEditingController: _passwordTextEditingController,
                        hint: 'Password',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w300,
                        color: Color(0XFF9391A4),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                clipBehavior: Clip.antiAlias,
                width: SizeConfig.scaleWidth(315),
                height: SizeConfig.scaleHeight(53),
                margin: EdgeInsets.only(
                  right: 30,
                  left: 30,
                  top: 30,
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(26.5),
                    color: Color(0XFF6A90F2)),
                child: ElevatedButton(
                  onPressed: () {},
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: SizeConfig.scaleHeight(20),
                      fontWeight: FontWeight.w500,
                      color: Color(0XFFFFFFFF),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
