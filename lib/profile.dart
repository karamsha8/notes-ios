import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';
import 'package:notse/text_field.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late TextEditingController _nameTextEditingController;
  late TextEditingController _emailTextEditingController;
  late TextEditingController _phoneTextEditingController;
  late TextEditingController _passwordTextEditingController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameTextEditingController = TextEditingController();
    _emailTextEditingController = TextEditingController();
    _phoneTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Profile',
          style: TextStyle(
            fontFamily: 'Quicksand ',
            fontSize: SizeConfig.scaleHeight(22),
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/setting');
              }),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(
          top: SizeConfig.scaleHeight(31),
          right: SizeConfig.scaleWidth(25),
          left: SizeConfig.scaleWidth(25),
        ),
        child: Column(
          children: [
            Container(
              height: SizeConfig.scaleHeight(65),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(4)),
                  boxShadow: [BoxShadow(offset: Offset(0, 2), blurRadius: .2)]),
              child: Padding(
                padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
                child: Row(
                  children: [
                    Stack(
                      children: [
                        CircleAvatar(
                          radius: SizeConfig.scaleWidth(27.5),
                          backgroundColor: Color(0xff6A90F2),
                          child: Text(
                            'M',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(20),
                              fontWeight: FontWeight.w400,
                              color: Color(0xffFFFFFF),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: SizeConfig.scaleHeight(10),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Momen Sisalem',
                                style: TextStyle(
                                  fontFamily: 'Quicksand ',
                                  fontSize: SizeConfig.scaleHeight(13),
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff111111),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(10),
                          ),
                          Row(
                            children: [
                              Text(
                                'momen.sisalem@gmail.com',
                                style: TextStyle(
                                  fontFamily: 'Quicksand ',
                                  fontSize: SizeConfig.scaleHeight(12),
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffA5A5A5),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(15),
            ),
            Row(
              children: [
                Container(
                  height: SizeConfig.scaleHeight(58),
                  width: SizeConfig.scaleWidth(85),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color(0xff6A90F2),
                      ),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [BoxShadow(offset: Offset(0, 1))]),
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: SizeConfig.scaleWidth(14),
                      right: SizeConfig.scaleWidth(13),
                      top: SizeConfig.scaleHeight(11),
                    ),
                    child: Column(
                      children: [
                        Text(
                          'Categories',
                          style: TextStyle(
                            fontFamily: 'Roboto ',
                            fontSize: SizeConfig.scaleHeight(12),
                            fontWeight: FontWeight.w500,
                            color: Color(0xff6A90F2),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(5),
                        ),
                        Text(
                          '14',
                          style: TextStyle(
                            fontFamily: 'Roboto ',
                            fontSize: SizeConfig.scaleHeight(12),
                            fontWeight: FontWeight.w500,
                            color: Color(0xffA5A5A5),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(58),
                  width: SizeConfig.scaleWidth(85),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color(0xff6A90F2),
                      ),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [BoxShadow(offset: Offset(0, 1))]),
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: SizeConfig.scaleWidth(14),
                      right: SizeConfig.scaleWidth(13),
                      top: SizeConfig.scaleHeight(11),
                    ),
                    child: Column(
                      children: [
                        Text(
                          'Done Notes',
                          style: TextStyle(
                            fontFamily: 'Roboto ',
                            fontSize: SizeConfig.scaleHeight(12),
                            fontWeight: FontWeight.w500,
                            color: Color(0xff6A90F2),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(5),
                        ),
                        Text(
                          '14',
                          style: TextStyle(
                            fontFamily: 'Roboto ',
                            fontSize: SizeConfig.scaleHeight(12),
                            fontWeight: FontWeight.w500,
                            color: Color(0xffA5A5A5),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(58),
                  width: SizeConfig.scaleWidth(85),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color(0xff6A90F2),
                      ),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [BoxShadow(offset: Offset(0, 1))]),
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: SizeConfig.scaleWidth(14),
                      right: SizeConfig.scaleWidth(13),
                      top: SizeConfig.scaleHeight(11),
                    ),
                    child: Column(
                      children: [
                        Text(
                          'Waiting Notes',
                          style: TextStyle(
                            fontFamily: 'Roboto ',
                            fontSize: SizeConfig.scaleHeight(12),
                            fontWeight: FontWeight.w500,
                            color: Color(0xff6A90F2),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(5),
                        ),
                        Text(
                          '14',
                          style: TextStyle(
                            fontFamily: 'Roboto ',
                            fontSize: SizeConfig.scaleHeight(12),
                            fontWeight: FontWeight.w500,
                            color: Color(0xffA5A5A5),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(15),
            ),
            Container(
              width: double.infinity,
              height: SizeConfig.scaleHeight(212),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.white,
                  boxShadow: [BoxShadow(offset: Offset(0, 1))]),
              child: Column(
                children: [
                  MyTextField(
                    textEditingController: _nameTextEditingController,
                    hint: 'Momen',
                    fontFamily: 'Quicksand',
                    fontSize: SizeConfig.scaleHeight(14),
                    fontWeight: FontWeight.w500,
                    color: Color(0XFF111111),
                  ),
                  MyTextField(
                    textEditingController: _emailTextEditingController,
                    hint: 'Sisalem',
                    fontFamily: 'Quicksand',
                    fontSize: SizeConfig.scaleHeight(14),
                    fontWeight: FontWeight.w300,
                    color: Color(0XFF111111),
                  ),
                  MyTextField(
                    textEditingController: _phoneTextEditingController,
                    hint: 'Phone',
                    fontFamily: 'Quicksand',
                    fontSize: SizeConfig.scaleHeight(14),
                    fontWeight: FontWeight.w400,
                    color: Color(0XFFA5A5A5),
                  ),
                  MyTextField(
                    textEditingController: _passwordTextEditingController,
                    hint: 'Password',
                    fontFamily: 'Quicksand',
                    fontSize: SizeConfig.scaleHeight(14),
                    fontWeight: FontWeight.w700,
                    color: Color(0XFFA5A5A5),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(15),
            ),
            Container(
              clipBehavior: Clip.antiAlias,
              width: double.infinity,
              height: SizeConfig.scaleHeight(46),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(13.25),
                  color: Color(0XFF6A90F2)),
              child: ElevatedButton(
                onPressed: () {},
                child: Text(
                  'Save',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: SizeConfig.scaleHeight(20),
                    fontWeight: FontWeight.w500,
                    color: Color(0XFFFFFFFF),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
