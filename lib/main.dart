import 'package:flutter/material.dart';
import 'package:notse/about_app.dart';
import 'package:notse/categories.dart';
import 'package:notse/category%20_name.dart';
import 'package:notse/launch_screen.dart';
import 'package:notse/new%20_category.dart';
import 'package:notse/new_note.dart';
import 'package:notse/profile.dart';
import 'package:notse/setting.dart';
import 'package:notse/sgin_up.dart';
import 'package:notse/sign%20_in.dart';
void  main()=>runApp(Main());
class Main extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute:'/launch_screen' ,
      routes: {
        '/launch_screen':(context)=> LaunchScreen(),
        '/sign_in':(context)=>SignIn(),
        '/sign_up':(context)=>SignUp(),
        '/categories':(context)=>Categories(),
        '/category_name':(context)=>CategoryName(),
        '/setting':(context)=>Setting(),
        '/profile':(context)=>Profile(),
        '/new_category':(context)=>NewCategory(),
        '/new_note':(context)=>NewNote(),
        '/about_app':(context)=>AboutApp(),
      },
    );
  }
}
