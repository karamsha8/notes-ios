import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';

class CategoryName extends StatefulWidget {
  const CategoryName({Key? key}) : super(key: key);

  @override
  _CategoryNameState createState() => _CategoryNameState();
}

class _CategoryNameState extends State<CategoryName> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Category Name',
          style: TextStyle(
            fontFamily: 'Quicksand ',
            fontSize: SizeConfig.scaleHeight(22),
            fontWeight: FontWeight.w300,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/categories');
              }),
          IconButton(
            icon: Icon(Icons.add_circle),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/new_note');
            },
            color: Colors.black,
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 17),
        children: [
          Card(
            elevation: 4,
            child: Row(
              children: [
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(
                      color: Color(0xff6A90F2),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                        topLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                      )),
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(13)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Note Title',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Lorem Ipsum is  ...',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: CircleAvatar(
                      radius: SizeConfig.scaleWidth(10),
                      backgroundColor: Color(0xffC4C4C4),
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    )),
                SizedBox(
                  width: SizeConfig.scaleWidth(11),
                ),
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(
                    color: Color(0xff6A90F2),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(SizeConfig.scaleWidth(4)),
                        bottomRight: Radius.circular(SizeConfig.scaleWidth(4)),
                      ),
                  ),
                ),
              ],
            ),
          ),
          Card(
            elevation: 4,
            child: Row(
              children: [
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(13)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Note Title',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Lorem Ipsum is  ...',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: CircleAvatar(
                      radius: SizeConfig.scaleWidth(10),
                      backgroundColor: Color(0xff98CE63),
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    )),
                SizedBox(
                  width: SizeConfig.scaleWidth(11),
                ),
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
              ],
            ),
          ),
          Card(
            elevation: 4,
            child: Row(
              children: [
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(13)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Note Title',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Lorem Ipsum is  ...',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: CircleAvatar(
                      radius: SizeConfig.scaleWidth(10),
                      backgroundColor: Color(0xffC4C4C4),
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    )),
                SizedBox(
                  width: SizeConfig.scaleWidth(11),
                ),
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
              ],
            ),
          ),
          Card(
            elevation: 4,
            child: Row(
              children: [
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(13)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Note Title',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Lorem Ipsum is  ...',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: CircleAvatar(
                      radius: SizeConfig.scaleWidth(10),
                      backgroundColor: Color(0xff98CE63),
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    )),
                SizedBox(
                  width: SizeConfig.scaleWidth(11),
                ),
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
              ],
            ),
          ),
          Card(
            elevation: 4,
            child: Row(
              children: [
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(13)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Note Title',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Lorem Ipsum is  ...',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: CircleAvatar(
                      radius: SizeConfig.scaleWidth(10),
                      backgroundColor: Color(0xffC4C4C4),
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    )),
                SizedBox(
                  width: SizeConfig.scaleWidth(11),
                ),
                Container(
                  height: SizeConfig.scaleHeight(115),
                  width: SizeConfig.scaleWidth(4),
                  decoration: BoxDecoration(color: Color(0xff6A90F2)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
