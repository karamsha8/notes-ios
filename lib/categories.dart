import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Categories',
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontWeight: FontWeight.w700,
            fontSize: SizeConfig.scaleHeight(22),
            color: Color(0xff474559),
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/setting');
            },
            color: Colors.black,
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushReplacementNamed(context, '/new_category');
        },
        child: Icon(
          Icons.add,
          size: SizeConfig.scaleHeight(17.95),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 17),
        children: [
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
              child: Row(
                children: [
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.scaleWidth(24),
                        child: Text(
                          'W',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.scaleHeight(22),
                            color: Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Work',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(13),
                                color: Color(0xff111111),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(10),
                        ),
                        Row(
                          children: [
                            Text(
                              'notes for work...',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(12),
                                color: Color(0xffA5A5A5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: Icon(
                      Icons.delete,
                      size: SizeConfig.scaleHeight(11.81),
                      color: Color(0xffD84040),
                    ),
                  ),
                  Container(
                      height: SizeConfig.scaleHeight(69.5),
                      width: SizeConfig.scaleWidth(20),
                      decoration: BoxDecoration(
                        color: Color(0xff6A90F2),
                        borderRadius: BorderRadius.only(
                          bottomRight:
                              Radius.circular(SizeConfig.scaleWidth(6)),
                          topRight: Radius.circular(SizeConfig.scaleWidth(6)),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: SizeConfig.scaleHeight(10),
                          color: Color(0xffFFFFFF),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/category_name');
                        },
                      )),
                ],
              ),
            ),
          ),
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
              child: Row(
                children: [
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.scaleWidth(24),
                        child: Text(
                          'W',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.scaleHeight(22),
                            color: Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Work',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(13),
                                color: Color(0xff111111),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(10),
                        ),
                        Row(
                          children: [
                            Text(
                              'notes for work...',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(12),
                                color: Color(0xffA5A5A5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: Icon(
                      Icons.delete,
                      size: SizeConfig.scaleHeight(11.81),
                      color: Color(0xffD84040),
                    ),
                  ),
                  Container(
                      height: SizeConfig.scaleHeight(69.5),
                      width: SizeConfig.scaleWidth(20),
                      decoration: BoxDecoration(
                        color: Color(0xff6A90F2),
                        borderRadius: BorderRadius.only(
                          bottomRight:
                          Radius.circular(SizeConfig.scaleWidth(6)),
                          topRight: Radius.circular(SizeConfig.scaleWidth(6)),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: SizeConfig.scaleHeight(10),
                          color: Color(0xffFFFFFF),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/category_name');
                        },
                      )),
                ],
              ),
            ),
          ),
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
              child: Row(
                children: [
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.scaleWidth(24),
                        child: Text(
                          'W',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.scaleHeight(22),
                            color: Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Work',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(13),
                                color: Color(0xff111111),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(10),
                        ),
                        Row(
                          children: [
                            Text(
                              'notes for work...',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(12),
                                color: Color(0xffA5A5A5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: Icon(
                      Icons.delete,
                      size: SizeConfig.scaleHeight(11.81),
                      color: Color(0xffD84040),
                    ),
                  ),
                  Container(
                      height: SizeConfig.scaleHeight(69.5),
                      width: SizeConfig.scaleWidth(20),
                      decoration: BoxDecoration(
                        color: Color(0xff6A90F2),
                        borderRadius: BorderRadius.only(
                          bottomRight:
                          Radius.circular(SizeConfig.scaleWidth(6)),
                          topRight: Radius.circular(SizeConfig.scaleWidth(6)),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: SizeConfig.scaleHeight(10),
                          color: Color(0xffFFFFFF),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/category_name');
                        },
                      )),
                ],
              ),
            ),
          ),
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
              child: Row(
                children: [
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.scaleWidth(24),
                        child: Text(
                          'W',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.scaleHeight(22),
                            color: Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Work',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(13),
                                color: Color(0xff111111),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(10),
                        ),
                        Row(
                          children: [
                            Text(
                              'notes for work...',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(12),
                                color: Color(0xffA5A5A5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: Icon(
                      Icons.delete,
                      size: SizeConfig.scaleHeight(11.81),
                      color: Color(0xffD84040),
                    ),
                  ),
                  Container(
                      height: SizeConfig.scaleHeight(69.5),
                      width: SizeConfig.scaleWidth(20),
                      decoration: BoxDecoration(
                        color: Color(0xff6A90F2),
                        borderRadius: BorderRadius.only(
                          bottomRight:
                          Radius.circular(SizeConfig.scaleWidth(6)),
                          topRight: Radius.circular(SizeConfig.scaleWidth(6)),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: SizeConfig.scaleHeight(10),
                          color: Color(0xffFFFFFF),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/category_name');
                        },
                      )),
                ],
              ),
            ),
          ),
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
              child: Row(
                children: [
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.scaleWidth(24),
                        child: Text(
                          'W',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.scaleHeight(22),
                            color: Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Work',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(13),
                                color: Color(0xff111111),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(10),
                        ),
                        Row(
                          children: [
                            Text(
                              'notes for work...',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(12),
                                color: Color(0xffA5A5A5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: Icon(
                      Icons.delete,
                      size: SizeConfig.scaleHeight(11.81),
                      color: Color(0xffD84040),
                    ),
                  ),
                  Container(
                      height: SizeConfig.scaleHeight(69.5),
                      width: SizeConfig.scaleWidth(20),
                      decoration: BoxDecoration(
                        color: Color(0xff6A90F2),
                        borderRadius: BorderRadius.only(
                          bottomRight:
                          Radius.circular(SizeConfig.scaleWidth(6)),
                          topRight: Radius.circular(SizeConfig.scaleWidth(6)),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: SizeConfig.scaleHeight(10),
                          color: Color(0xffFFFFFF),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/category_name');
                        },
                      )),
                ],
              ),
            ),
          ),
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(left: SizeConfig.scaleWidth(15)),
              child: Row(
                children: [
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: SizeConfig.scaleWidth(24),
                        child: Text(
                          'W',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.scaleHeight(22),
                            color: Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Work',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(13),
                                color: Color(0xff111111),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(10),
                        ),
                        Row(
                          children: [
                            Text(
                              'notes for work...',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.scaleHeight(12),
                                color: Color(0xffA5A5A5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.scaleHeight(69.5),
                    child: Icon(
                      Icons.delete,
                      size: SizeConfig.scaleHeight(11.81),
                      color: Color(0xffD84040),
                    ),
                  ),
                  Container(
                      height: SizeConfig.scaleHeight(69.5),
                      width: SizeConfig.scaleWidth(20),
                      decoration: BoxDecoration(
                        color: Color(0xff6A90F2),
                        borderRadius: BorderRadius.only(
                          bottomRight:
                          Radius.circular(SizeConfig.scaleWidth(6)),
                          topRight: Radius.circular(SizeConfig.scaleWidth(6)),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: SizeConfig.scaleHeight(10),
                          color: Color(0xffFFFFFF),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/category_name');
                        },
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
