import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';
class AboutApp extends StatefulWidget {
  const AboutApp({Key? key}) : super(key: key);

  @override
  _AboutAppState createState() => _AboutAppState();
}

class _AboutAppState extends State<AboutApp> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Stack(
        children: [
          Image(
            image: AssetImage('images/launch.png'),
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Center(
            child: Container(
              height: SizeConfig.scaleHeight(250),
              width: SizeConfig.scaleWidth(250),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Color(0xff6A90F2),
                )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 60,
                    child: Image(
                      image: AssetImage('images/kk.jpg'),
                    ),
                  ),
                  Text(
                    'My Notes',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Roboto ',
                      fontSize: SizeConfig.scaleHeight(30),
                      fontWeight: FontWeight.w700,
                      color: Color(0xff23203F),
                    ),
                  ),
                  Text(
                    'For Organized Life',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Roboto ',
                      fontSize: SizeConfig.scaleHeight(15),
                      fontWeight: FontWeight.w300,
                      color: Color(0xff707070),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            right:SizeConfig.scaleWidth(0),
            left:SizeConfig.scaleWidth(0),
            bottom: SizeConfig.scaleHeight(20),
            child:Text(
              'ios Course - Notes App V1.0 ',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Roboto ',
                fontSize: SizeConfig.scaleHeight(15),
                fontWeight: FontWeight.w300,
                color: Color(0xff707070),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
