import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';
import 'package:notse/text_field.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  late TapGestureRecognizer _tapGestureRecognizer;
  late TextEditingController _emailTextEditingController;
  late TextEditingController _passwordTextEditingController;

  String? _emailError;
  String? _passwordError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tapGestureRecognizer = TapGestureRecognizer()..onTap = navigateToRegister;
    // _tapGestureRecognizer.onTap = navigateToRegister;

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _tapGestureRecognizer.dispose();
    _emailTextEditingController.dispose();
    _passwordTextEditingController.dispose();
  }

  void navigateToRegister() =>
      Navigator.pushReplacementNamed(context, '/sign_up');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Image(
          image: AssetImage('images/launch.png'),
          width: double.infinity,
          height: double.infinity,
          fit: BoxFit.cover,
        ),
        Padding(
          padding: EdgeInsets.only(
            top: SizeConfig.scaleHeight(106),
            right: SizeConfig.scaleHeight(27),
            left: SizeConfig.scaleHeight(27),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // SizedBox(
              //   height: SizeConfig.scaleHeight(106),
              //   width: SizeConfig.scaleHeight(27),
              // ),
              Text(
                'Sign In',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Nunito',
                  fontSize: SizeConfig.scaleHeight(30),
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'Login to start app',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: SizeConfig.scaleHeight(18),
                  fontWeight: FontWeight.w300,
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(81),
              ),
              Container(
                height: SizeConfig.scaleHeight(180),
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(9),
                    color: Colors.white),
                child: Column(
                  children: [
                    SizedBox(
                      height: SizeConfig.scaleHeight(32),
                      width: SizeConfig.scaleHeight(21),
                    ),
                    MyTextField(
                      textEditingController: _emailTextEditingController,
                      hint: 'Kendrick',
                      fontFamily: 'Roboto',
                      fontSize: SizeConfig.scaleHeight(22),
                      fontWeight: FontWeight.w400,
                      color: Color(0XFF23203F),
                    ),
                    SizedBox(
                      height: SizeConfig.scaleHeight(30),
                    ),
                    MyTextField(
                      textEditingController: _passwordTextEditingController,
                      hint: 'Password',
                      fontFamily: 'Roboto',
                      fontSize: SizeConfig.scaleHeight(22),
                      fontWeight: FontWeight.w300,
                      color: Color(0XFF9391A4),
                      obscureText: true,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(30),
                width: SizeConfig.scaleWidth(30),
              ),
              Container(
                clipBehavior: Clip.antiAlias,
                width: SizeConfig.scaleWidth(315),
                height: SizeConfig.scaleHeight(53),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(17),
                    color: Color(0XFF6A90F2)),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/categories');
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: SizeConfig.scaleHeight(26.5),
                      fontWeight: FontWeight.w500,
                      color: Color(0XFFFFFFFF),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(10),
                width: SizeConfig.scaleHeight(64),
              ),
              Center(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'Don\'t have an account?',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: SizeConfig.scaleHeight(18),
                      fontWeight: FontWeight.w300,
                      color: Color(0XFF9391A4),
                    ),
                    children: [
                      TextSpan(text: ' '),
                      TextSpan(
                        recognizer: _tapGestureRecognizer,
                        text: 'Sign up',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: SizeConfig.scaleHeight(18),
                          fontWeight: FontWeight.w500,
                          color: Color(0XFF23203F),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  void performLogin() {
    if (checkData()) {
      //TODO:NAVIGATE TO HOME SCREEN
    }
  }

  //TODO:CHECK IF FIELDS HAVE DATA
  bool checkData() {
    if (_emailTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty) {
      checkFieldsError();
      showMessage('Login success');
      Future.delayed(Duration(seconds: 3), () {
        Navigator.pushReplacementNamed(context, '/main_screen');
      });
      return true;
    }
    checkFieldsError();
    //TODO:SHOW ERROR MESSAGE IN CASE OF ERROR DATA
    showMessage('Please, enter required data', error: true);
    return false;
  }

  void checkFieldsError() {
    setState(() {
      _emailError = _emailTextEditingController.text.isEmpty
          ? 'Enter email address'
          : null;
      _passwordError =
          _passwordTextEditingController.text.isEmpty ? 'Enter password' : null;
    });
  }

  void showMessage(String message, {bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        content: Text(
          message,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        duration: Duration(seconds: 2),
        backgroundColor: error ? Colors.red : Colors.green,
      ),
    );
  }
}
