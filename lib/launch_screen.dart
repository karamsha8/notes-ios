import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      Navigator.pushReplacementNamed(context, '/sign_in');
    });
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Stack(
        children: [
          Image(
            image: AssetImage('images/launch.png'),
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Center(
            child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 60,
                  child: Image(
                    image: AssetImage('images/kk.jpg'),
                  ),
                ),
                Text(
                  'My Notes',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize:SizeConfig.scaleTextFont(30),

                  ),
                ),
                Text(
                  'For Organized Life',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize:SizeConfig.scaleTextFont(10),
                  ),
                )
              ],
            ),
            ),
          ),
          Positioned(
            right:SizeConfig.scaleWidth(0),
            left:SizeConfig.scaleWidth(0),
            bottom: SizeConfig.scaleHeight(20),
            child:Text(
              'ios Course - Notes App V1.0 ',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize:SizeConfig.scaleTextFont(10),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
