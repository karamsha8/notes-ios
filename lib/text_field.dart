import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';

class MyTextField extends StatelessWidget {
  final TextEditingController textEditingController;
  final String? errorText;
  final TextInputType textInputType;
  final String hint;
  final bool obscureText;
  final String fontFamily;
  final double fontSize;
  final FontWeight fontWeight;
  final Color color;

  MyTextField({
    required this.textEditingController,
    required this.hint,
    this.errorText,
    this.obscureText = false,
    this.textInputType = TextInputType.text,
    required this.fontFamily,
    required this.fontSize,
    required this.fontWeight,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(

      keyboardType: textInputType,
      obscureText: obscureText,
      controller: textEditingController,
      // maxLength: 50,
      maxLines: 1,
      decoration: InputDecoration(
        fillColor: Colors.transparent,
        // labelText: 'Email'
        counterText: '',
        filled: true,
        hintText: hint,
        hintStyle: TextStyle(
          fontFamily: fontFamily,
          fontSize: fontSize,
          fontWeight: fontWeight,
          color:color,
        ),
        errorText: errorText,
        errorMaxLines: 1,

      ),
    );
  }

  }

