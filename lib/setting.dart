import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';

class Setting extends StatefulWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Setting',
          style: TextStyle(
            fontFamily: 'Quicksand ',
            fontSize: SizeConfig.scaleHeight(22),
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/categories');
              }),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: SizeConfig.scaleWidth(18),
          right: SizeConfig.scaleWidth(17),
        ),
        child: Column(
          children: [
            CircleAvatar(
              radius: SizeConfig.scaleWidth(35.5),
              child: Text(
                'M',
                style: TextStyle(
                  fontFamily: 'Quicksand ',
                  fontSize: SizeConfig.scaleHeight(24),
                  fontWeight: FontWeight.w400,
                  color: Color(0xffFFFFFF),
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(8),
            ),
            Text(
              'Momen Sisalem',
              style: TextStyle(
                fontFamily: 'Quicksand ',
                fontSize: SizeConfig.scaleHeight(15),
                fontWeight: FontWeight.w500,
                color: Color(0xff111111),
              ),
            ),
            Text(
              'momen.sisalem@gmail.com',
              style: TextStyle(
                fontFamily: 'Quicksand ',
                fontSize: SizeConfig.scaleHeight(13),
                fontWeight: FontWeight.w500,
                color: Color(0xffA5A5A5),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(10),
            ),
            Divider(
              color: Color(0xffD0D0D0),
              endIndent: SizeConfig.scaleWidth(28),
              indent: SizeConfig.scaleWidth(27),
              height: SizeConfig.scaleHeight(1),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(25),
            ),
            Card(
              elevation: 4,
              child: Row(children: [
                Container(
                  height: SizeConfig.scaleHeight(70),
                  width: SizeConfig.scaleWidth(5),
                  decoration: BoxDecoration(
                    color: Color(0xff6A90F2),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                      topLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                    ),
                  ),
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(10),
                ),
                Stack(
                  children: [
                    CircleAvatar(
                      radius: SizeConfig.scaleWidth(24),
                      child: Icon(Icons.language),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Language',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xff111111),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Selected language: EN',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                              color: Color(0xffA5A5A5),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(69.5),
                  child: IconButton(
                    icon: Icon(Icons.chevron_right),
                    onPressed: () {

                    },
                  ),
                ),
              ]),
            ),
            Card(
              elevation: 4,
              child: Row(children: [
                SizedBox(
                  width: SizeConfig.scaleWidth(15),
                ),
                Stack(
                  children: [
                    CircleAvatar(
                      radius: SizeConfig.scaleWidth(24),
                      child: Icon(Icons.person),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Profile',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xff111111),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Update your data…',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                              color: Color(0xffA5A5A5),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(69.5),
                  child: IconButton(
                    icon: Icon(Icons.chevron_right),
                    onPressed: () {
                      Navigator.pushNamed(context, '/profile');
                    },
                  ),
                ),
                Container(
                  height: SizeConfig.scaleHeight(70),
                  width: SizeConfig.scaleWidth(5),
                  decoration: BoxDecoration(
                    color: Color(0xff6A90F2),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(SizeConfig.scaleWidth(4)),
                      topRight: Radius.circular(SizeConfig.scaleWidth(4)),
                    ),
                  ),
                ),
              ]),
            ),
            Card(
              elevation: 4,
              child: Row(children: [
                Container(
                  height: SizeConfig.scaleHeight(70),
                  width: SizeConfig.scaleWidth(5),
                  decoration: BoxDecoration(
                    color: Color(0xff6A90F2),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                      topLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                    ),
                  ),
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(10),
                ),
                Stack(
                  children: [
                    CircleAvatar(
                      radius: SizeConfig.scaleWidth(24),
                      child: Icon(Icons.system_update_alt),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'About App',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xff111111),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'What is notes app?',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                              color: Color(0xffA5A5A5),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(69.5),
                  child: IconButton(
                    icon: Icon(Icons.chevron_right),
                    onPressed: (){
                      Navigator.pushNamed(context,  '/about_app');
                    },
                  ),
                ),
              ]),
            ),
            Card(
              elevation: 4,
              child: Row(children: [
                SizedBox(
                  width: SizeConfig.scaleWidth(15),
                ),
                Stack(
                  children: [
                    CircleAvatar(
                      radius: SizeConfig.scaleWidth(24),
                      child: Icon(Icons.info),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'About course',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xff111111),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Describe the course in brief',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                              color: Color(0xffA5A5A5),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(69.5),
                  child: IconButton(
                    icon: Icon(Icons.chevron_right),
                    onPressed: (){
                      Navigator.pushNamed(context,  '/profile');
                    },
                  ),
                ),
                Container(
                  height: SizeConfig.scaleHeight(70),
                  width: SizeConfig.scaleWidth(5),
                  decoration: BoxDecoration(
                    color: Color(0xff6A90F2),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(SizeConfig.scaleWidth(4)),
                      topRight: Radius.circular(SizeConfig.scaleWidth(4)),
                    ),
                  ),
                ),
              ]),
            ),
            Card(
              elevation: 4,
              child: Row(children: [
                Container(
                  height: SizeConfig.scaleHeight(70),
                  width: SizeConfig.scaleWidth(5),
                  decoration: BoxDecoration(
                    color: Color(0xff6A90F2),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                      topLeft: Radius.circular(SizeConfig.scaleWidth(4)),
                    ),
                  ),
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(10),
                ),
                Stack(
                  children: [
                    CircleAvatar(
                      radius: SizeConfig.scaleWidth(24),
                      child: Icon(Icons.power_settings_new),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.scaleWidth(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Logout',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(13),
                              fontWeight: FontWeight.w500,
                              color: Color(0xff111111),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      Row(
                        children: [
                          Text(
                            'Waiting your return…',
                            style: TextStyle(
                              fontFamily: 'Quicksand ',
                              fontSize: SizeConfig.scaleHeight(12),
                              fontWeight: FontWeight.w500,
                              color: Color(0xffA5A5A5),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                  height: SizeConfig.scaleHeight(69.5),
                  child: IconButton(
                    icon: Icon(Icons.chevron_right),
                    onPressed: (){
                    },
                  ),
                ),
              ]),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(134),
            ),
            Positioned(
              right: SizeConfig.scaleWidth(0),
              left: SizeConfig.scaleWidth(0),
              bottom: SizeConfig.scaleHeight(14),
              child: Text(
                'ios Course - Notes App V1.0 ',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Quicksand ',
                  fontSize: SizeConfig.scaleHeight(12),
                  fontWeight: FontWeight.w500,
                  color: Color(0xffA5A5A5),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
