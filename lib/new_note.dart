import 'package:flutter/material.dart';
import 'package:notse/size_config.dart';
import 'package:notse/text_field.dart';
class NewNote extends StatefulWidget {
  const NewNote({Key? key}) : super(key: key);

  @override
  _NewNoteState createState() => _NewNoteState();
}

class _NewNoteState extends State<NewNote> {
  late TextEditingController _nameTextEditingController;
  late TextEditingController _lastNameTextEditingController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameTextEditingController = TextEditingController();
    _lastNameTextEditingController = TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
              left: SizeConfig.scaleWidth(33),
              top: SizeConfig.scaleWidth(22),
              child: IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () {
                    Navigator.pushNamed(context, '/category_name');
                  })),

          Padding(
            padding:EdgeInsets.only(
              top: SizeConfig.scaleHeight(92),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'New Note',
                  style: TextStyle(
                    fontFamily: 'Nunito',
                    fontSize: SizeConfig.scaleHeight(30),
                    fontWeight: FontWeight.w700,
                    color: Color(0XFF23203F),
                  ),
                ),
                Text(
                  'Create note',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: SizeConfig.scaleHeight(18),
                    fontWeight: FontWeight.w300,
                    color: Color(0XFF9391A4),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(42),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    right: SizeConfig.scaleWidth(25),
                    left: SizeConfig.scaleWidth(25),
                    bottom: SizeConfig.scaleHeight(20),
                    top: SizeConfig.scaleHeight(20),
                  ),
                  child: Column(
                    children: [
                      MyTextField(
                        textEditingController: _nameTextEditingController,
                        hint: 'Note Tile',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w400,
                        color: Color(0XFF23203F),
                      ),
                      MyTextField(
                        textEditingController: _lastNameTextEditingController,
                        hint: 'Description',
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(22),
                        fontWeight: FontWeight.w300,
                        color: Color(0XFF9391A4),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(15),
                ),
                Container(
                  clipBehavior: Clip.antiAlias,
                  width: double.infinity,
                  height: SizeConfig.scaleHeight(53),
                  margin: EdgeInsets.only(
                    right: 30,
                    left: 30,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(26.5),
                      color: Color(0XFF6A90F2)),
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text(
                      'Save',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: SizeConfig.scaleHeight(20),
                        fontWeight: FontWeight.w500,
                        color: Color(0XFFFFFFFF),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
